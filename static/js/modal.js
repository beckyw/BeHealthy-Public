var modal = {
    init: function() {
        // show modal when the create review button is clicked
        $('.create-review').click(function(e) {
            e.preventDefault();
            $('#create-review-modal').modal('show')   
        });

        // reset fields on modal close
        $('#create-review-modal').on('hidden.bs.modal', function (e) {
            // reset all input fields
            $('#create-review-modal input').val('');
            // reset all select dropdown field to default option
            $('#create-review-modal select').prop('selectedIndex', 0);
            // remove textarea fields
            $('#create-review-modal .radio input').prop('checked', false);
            // remove textarea fields
            $('#create-review-modal textarea').val('');
        });

        $('form.new-review').on('submit', function(e) {
            e.preventDefault();

            var name = $('#reviewer').val();
            var product = $('#product').val();
            var rating = $('input[name="ratings"]:checked').val();
            var comments = $('#comments').val();

            var review = {
                "reviewer": name,
                "product": product,
                "rating": parseInt(rating),
                "comments": comments
            }

            $.ajax({
                url: "/add-review",
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(review),
                success: function(data, status, jqXHR) {
                    $('#create-review-modal').modal('hide');
                    $('#modal-success').find('.modal-body').text("Success! Your review has been added!");
                    $('#modal-success').modal('show');
                },
                error: function(jqXHR, status, error) {
                    console.log(status + ", " + error);
                    console.log(jqXHR);
                    $('#create-review-modal').addClass("change-z");
                    $('#modal-error').find('.modal-body').text("Error! Your review cannot be submitted. Please try again.");
                    $('#modal-error').modal('show');
                }
            });
        });

        // remove z-index css on create review modal
        $('#modal-error').on('hidden.bs.modal', function (e) {
            $('#create-review-modal').removeClass("change-z");
        });

        // reload review page on success ok
        $('#modal-success').on('hidden.bs.modal', function (e) {
            location.reload();
        });
    }
}

$(document).ready(function() {'use strict';
    modal.init();
});