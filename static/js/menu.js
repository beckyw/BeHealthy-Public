var menu = {
    init: function() {
        // show modal when the create review button is clicked
        $('.menu-button').click(function(e) {
            console.log("Clicked");
            e.preventDefault();
            $('.menu-container').slideToggle();
            if ($(this).hasClass('menu-open')) {
                $(this).removeClass('menu-open');
            } else {
                $(this).addClass('menu-open');
            }
        });
    }
}

$(document).ready(function() {'use strict';
    menu.init();
});