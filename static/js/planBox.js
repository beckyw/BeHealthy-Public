var planBox = {
    init: function() {
        // resize boxes
        planBox.resizeBox();

        // resize boxes on window resize
        $( window ).resize(function() {
            // reset height
            $('.body-content .insurance > div').each(function() {
                $(this).css("height","");
            });
            // runs at less than LG breakpoint of 992px
            if($(window).width() > 992) {
                planBox.resizeBox();
            }
        });

    },
    resizeBox: function() {
        var max = 0;

        // get the max height
        $('.body-content .insurance > div').each(function() {
            var height = $(this).outerHeight();
            console.log(height);
            if (height > max) {
                max = height;
            }
        });

        console.log(max);

        // update heights
        $('.body-content .insurance').each(function() {
            $(this).children('div').outerHeight(max);
        });
    }
}

$(document).ready(function() {'use strict';
    planBox.init();
});