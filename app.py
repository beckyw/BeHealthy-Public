import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash

'''
----------------------------------------------------------
CONFIGURATION
----------------------------------------------------------
'''
DATABASE = 'db/reviews.db'

'''
INITIALISE web app
'''
app = Flask(__name__)
app.config.from_pyfile('app.cfg')


'''
----------------------------------------------------------
DATABASE functions
----------------------------------------------------------
'''
# connects to the database
def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

# disconnect from the database after each request
# (if there is a connection to the database)
@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

# function to get the database object
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_db()
    return db

# function to query the database
def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

'''
----------------------------------------------------------
ROUTES for the website
----------------------------------------------------------
'''
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/insurance')
def insurance():
    return render_template('insurance.html')

@app.route('/reviews')
def reviews():
    # connext to the database to get the reviews
    #g.db = connect_db()
    reviews = query_db('select * from reviews order by datetime(date) desc')
    return render_template('reviews.html', reviews=reviews)

@app.route('/add-review', methods=['POST'])
def add_review():
    g.db = connect_db()
    review = request.get_json()
    print review
    g.db.execute('insert into reviews (reviewer, product, rating, comments) values (?, ?, ?, ?)',
                 [review['reviewer'], review['product'], review['rating'], review['comments']])
    g.db.commit()
    # returns intentional empty response
    return ('', 204)


@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

'''
----------------------------------------------------------
MAIN
----------------------------------------------------------
'''
if __name__ == '__main__':
    app.run()