drop table if exists reviews;
create table reviews (
  id integer primary key autoincrement,
  date datetime default CURRENT_TIMESTAMP not null,
  reviewer text not null,
  product text not null,
  rating int not null,
  comments text not null
);
insert into reviews(reviewer, comments, product, rating) values('Peter', 'Great insurance for the family, covers many things. Would recommend this.', 'Family Insurance', 5);
insert into reviews(reviewer, comments, product, rating) values('Becky', 'This is a great product, suits me just right. Be Healthy has been easy to deal with. Only issue I have is that they do not have major dental included in this plan, otherwise it is everything I need!', 'Singles Insurance', 4);